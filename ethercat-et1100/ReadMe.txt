******************************************************
EtherCAT-ET1100 demo on C2000 + AMIC110
******************************************************

The detailed user guide is at
http://ap-fpdsp-swapps.dal.design.ti.com/index.php/EtherCAT_DDRless_on_AMIC110_C2000_for_Industrial_Applications


This folder contains PDK thumb mode library patch and custom controlSuite patch file which is required to be applied 
on top of controlSuite package. The c2000_esc_et1100.patch is required to enable EtherCAT on AMIC110 with C2000 
DelfinoMCUs F28379D LaunchPad Development Kit. The patch is expected to be incorporated officially in the upcoming 
releases of controlSuite package.

User must make sure that these patch is applied on recommended version of controlSuite package.
Refer to SPRUIG9 for more details.

The controlSuite patch as well as PDK patch for thumb mode libraries are applied using the script escSetup.bat.
The script also build the thumb mode libraries of PDK after its patch is applied.


**************************************************
BUILDING AMIC110 EtherCAT ET1100 application
**************************************************
Refer to http://processors.wiki.ti.com/index.php/PRU_ICSS_EtherCAT

**************************************************
BUILDING TMDSECATCNCD379D_EtherCAT_Reference
**************************************************
After applying the patch, refer to SPRUIG9 section 7 - Importing and Building CCS Solution Reference Project Using CCS GUI,
select build configuration:
_5_F2837xD_LAUNCHPAD_SPIA_FLASH: 
C2000 EtherCAT slave software running from FLASH on Delfino(F2837xD) 
Launchpad using SPIA (J1, J2,J43, J4 connectors) to i/f with AMIC110 boosterpack

Or
_6_F2837xD_LAUNCHPAD_SPIA_RAM:
C2000 EtherCAT slave software running from RAM on Delfino(F2837xD) Launchpad using SPIA (J1, J2,J43, J4 connectors) to i/f
with AMIC110 boosterpack

