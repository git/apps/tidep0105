@REM ***********************************************************************************
@REM * FILE PURPOSE: Rebuild arm mode PDK lib
@REM ***********************************************************************************
@REM * FILE NAME: rebuildPDK.bat
@REM *
@REM * DESCRIPTION: 
@REM *  Rebuild arm mode PDK libraries. Some PDK libraries are built in thumb mode for DDR-less
@REM *  EtherCAT. Using this script to recover to arm mode PDK libraries.
@REM *  The batch file requires Windows patch utility from gnuwin32 sourceforge. 
@REM *  (Note that this is not a TI tool - See licensing information page for more details:
@REM *   http://savannah.gnu.org/projects/patch/)
@REM *
@REM * USAGE:
@REM *  rebuildPDK.bat
@REM *
@REM * Copyright (C) 2012-2017, Texas Instruments, Inc.
@REM *****************************************************************************

::Turn off console prints including this command
@echo off
set "ESC_ET1100_DEMO_PATH=%~dp0"
set "AM335x_PDK_INSTALL_PATH=C:\ti\pdk_am335x_1_0_8\packages"
set "PATCH_BIN_INSTALL_PATH=C:\patch-2.5.9-7-bin\bin"

echo.
echo revert PDK patch
%PATCH_BIN_INSTALL_PATH%\patch -i %ESC_ET1100_DEMO_PATH%\patches\pdk_1_0_8.patch -p2 -R -d %AM335x_PDK_INSTALL_PATH%

echo Build arm mode PDK libraries...
cd %AM335x_PDK_INSTALL_PATH%
call pdksetupenv.bat
gmake csl_clean osal_clean board_clean pruss_clean gpio_clean spi_clean uart_clean
gmake csl osal board pruss gpio spi uart
cd %ESC_ET1100_DEMO_PATH%