@REM ***********************************************************************************
@REM * FILE PURPOSE: Apply patches for EtherCAT ET1100 demo and build thumb mode PDK lib
@REM ***********************************************************************************
@REM * FILE NAME: escsetup.bat
@REM *
@REM * DESCRIPTION: 
@REM *  Apply PDK and C2000 patches and build required thumb mode PDK libraries.
@REM *
@REM *  The batch file requires Windows patch utility from gnuwin32 sourceforge. 
@REM *  (Note that this is not a TI tool - See licensing information page for more details:
@REM *   http://savannah.gnu.org/projects/patch/)
@REM *
@REM * USAGE:
@REM *  escsetup.bat
@REM *
@REM * Copyright (C) 2012-2017, Texas Instruments, Inc.
@REM *****************************************************************************

@echo off
set "ESC_ET1100_DEMO_PATH=%~dp0"
set "PATCH_BIN_INSTALL_PATH=C:\patch-2.5.9-7-bin\bin"
set "AM335x_PDK_INSTALL_PATH=C:\ti\pdk_am335x_1_0_8\packages"
set "CONTROLSUITE_INSTALL_PATH=C:\ti\controlSUITE"
set "C2000_ESC_REF_PATH=development_kits\TMDSECATCND379D_V1.0\TMDSECATCNCD379D_EtherCAT_Solution_Ref"

echo.
echo Appy PDK patch... 
::%PATCH_BIN_INSTALL_PATH%\patch -i %ESC_ET1100_DEMO_PATH%\patches\pdk_1_0_8.patch -p2 -d %AM335x_PDK_INSTALL_PATH%

echo Appy C2000 patch... 
%PATCH_BIN_INSTALL_PATH%\patch -i %ESC_ET1100_DEMO_PATH%\patches\c2000_esc_et1100.patch -p8 -d %CONTROLSUITE_INSTALL_PATH%\%C2000_ESC_REF_PATH%

echo Build thumb mode PDK libraries...
cd %AM335x_PDK_INSTALL_PATH%
call pdksetupenv.bat
gmake csl_clean osal_clean board_clean pruss_clean gpio_clean spi_clean uart_clean
gmake csl osal board pruss gpio spi uart
cd %ESC_ET1100_DEMO_PATH%

